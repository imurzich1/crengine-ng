These unit tests use the following fonts:

FreeFont (Free UCS Outline Fonts):
  URL: https://www.gnu.org/software/freefont/
  Version: 20120503
  License: GPLv3
