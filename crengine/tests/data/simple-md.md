# HEAD 1

## HEAD 2

### HEAD 3

Setext head 1
===

Setext head 2
---


* markered list item 1
* markered list item 2
* markered list item 3
* markered list item 4

1. number list item 1
2. number list item 2
3. number list item 3
4. number list item 4

Regular **Bold** *Italic* ***BoldItalic*** <u>Underline</u> <u>**BoldUnderline**</u> <u>_ItalicUnderline_</u> <u>***BoldItalicUnderline***</u> ~~Strikethrough~~ <u>~~StrikethroughUnderline~~</u>.

|  col1 head  |  col2 head  |
| ----------- | ----------- |
| data11      | data12      |
| data21      |      data22 |

<table border="1">
<tr>
<td>11</td>
<td>12</td>
</tr>
<tr>
<td>21</td>
<td>22</td>
</tr>
</table>

e<sup>x</sup> x<sub>i</sub>

"a"xy

'ABCD'e

[Hyperlink](http://example.com/)

[Hyperlink with title](http://example.com/ "This is title")

![Image alt](img1.png)

![Image alt](img1.png "This is title")

```
<i>it</i>
```

    <strong>Bold</strong>

```perl
my $x ~= s/^([0-9]+).*$/\1/;
```

In some text `inline code foo bar()` line tail.

Users homepage: <https://www.autolink.com/>

Users homepage2: https://www.autolink2.com/

User's email: <user@domain.com>

> Some quote.
Quote continue.

Last line.
